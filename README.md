Simple script to demo Karbon

Follow the instruction below :

1. Create a karbon deployment and connect to the K8S cluster.
2. launch the command git clone git@gitlab.com:grelot/karbondemo.git
3. cd karbondemo
4. ./fulldeploy.sh
5. specify metallb IP range configuration (chose unused IP on the NTNX cluster)
6. specify a name for the loadbalancer service
7. Retrieve the external IP and paste it in a browser

