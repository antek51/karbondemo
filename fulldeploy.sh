#metallb install & secret create
echo "Installation de metallb"
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.9.5/manifests/namespace.yaml
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.9.5/manifests/metallb.yaml
kubectl create secret generic -n metallb-system memberlist --from-literal=secretkey="$(openssl rand -base64 128)"

#metal lb config
echo "Configuraion MetalLB"
#Recuper l'ip du master
IP=$(kubectl get nodes --selector='kubernetes.io/role=master' -o jsonpath='{.items[*].status.addresses[?(@.type=="InternalIP")].address}')
echo "LB Start IP : "
read IPSTART
export IPSTART=$IPSTART
echo "LB End IP : "
read IPEND
export IPEND=$IPEND
startIP=$(awk -F"." '{print $1"."$2"."$3".'$IPSTART'"}'<<<$IP)
echo "LB Start with :" $startIP
endIP=$(awk -F"." '{print $1"."$2"."$3".'$IPEND'"}'<<<$IP)
echo "LB ends with :" $endIP
export startIP=$startIP
export endIP=$endIP
cat metal.yaml| envsubst '${startIP} ${endIP}' | kubectl apply -f -

#Lancement du deploiement
echo "Déploiement application web"
kubectl apply -f deploy.yaml

#load balancer creation
echo "Deploiement du load balancer, Veuillez entrer un nom : "
read nameSVC
export nameSVC=$nameSVC
cat lb.yaml| envsubst '${nameSVC}' | kubectl apply -f -

kubectl get svc $nameSVC

